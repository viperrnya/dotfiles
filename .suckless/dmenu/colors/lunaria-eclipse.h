static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#dfe2ed", "#323f46" },
	[SchemeSel] = { "#2c313a", "#bedbc1" },
	[SchemeSelHighlight] = { "#bedbc1", "#7f9781" },
	[SchemeNormHighlight] = { "#bedbc1", "#7f9781" },
	[SchemeOut] = { "#000000", "#bcdbff" },
	[SchemeMid] = { "#d7d7d7", "#2c313a" },
};
