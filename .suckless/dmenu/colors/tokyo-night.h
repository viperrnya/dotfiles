static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#c0caf5", "#24283b" },
	[SchemeSel] = { "#2c313a", "#f7768e" },
	[SchemeSelHighlight] = { "#1d202f", "#7aa2f7" },
	[SchemeNormHighlight] = { "#1d202f", "#7aa2f7" },
	[SchemeOut] = { "#000000", "#bcdbff" },
	[SchemeMid] = { "#d7d7d7", "#1d202f" },
};
