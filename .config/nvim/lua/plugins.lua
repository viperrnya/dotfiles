require("lazy").setup({

    -- Start Screen --
    {
        'nvimdev/dashboard-nvim',
        event = 'VimEnter',
        dependencies = { {'nvim-tree/nvim-web-devicons'}}
    },

    -- Telescope --
    {
        "nvim-telescope/telescope.nvim",
        dependencies = { "nvim-lua/plenary.nvim" }
    },
    "nvim-telescope/telescope-file-browser.nvim",

    -- Syntax Highlighting --
    "nvim-treesitter/nvim-treesitter",

    -- Productivity --
	"vimwiki/vimwiki",
	"jreybert/vimagit",
	"nvim-orgmode/orgmode",

	"folke/which-key.nvim", -- Which Key
	"nvim-lualine/lualine.nvim", -- A better statusline

	-- File management --
	"vifm/vifm.vim",
	"scrooloose/nerdtree",
	"tiagofumo/vim-nerdtree-syntax-highlight",
	"ryanoasis/vim-devicons",

	-- Tim Pope Plugins --
	"tpope/vim-surround",

	-- Syntax Highlighting and Colors --
	"PotatoesMaster/i3-vim-syntax",
	"kovetskiy/sxhkd-vim",
	"vim-python/python-syntax",
	"ap/vim-css-color",
	"nickeb96/fish.vim",

	-- Junegunn Choi Plugins --
	"junegunn/goyo.vim",
	"junegunn/limelight.vim",
	"junegunn/vim-emoji",

	-- Colorschemes --
	"kyazdani42/nvim-palenight.lua",
	{
        "folke/tokyonight.nvim",
        lazy = false,
        priority = 1000,
    },

	-- Other stuff --
	"luochen1990/rainbow",

})