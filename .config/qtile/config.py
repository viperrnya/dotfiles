import os
import re
import socket
import subprocess
import colors
from libqtile import qtile
from libqtile import bar, extension, hook, layout, qtile, widget
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.command import lazy
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from typing import List  # noqa: F401
#Requires "qtile-extras"
from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration
#Spotify widget
from spotify import Spotify



mod = "mod4"              # Sets mod key to SUPER/WINDOWS
myTerm = "alacritty"      # My terminal of choice
myBrowser = "librewolf"   # My browser of choice
myFM = "pcmanfm"          # My file manager of choice

# Allows you to input a name when adding treetab section.
@lazy.layout.function
def add_treetab_section(layout):
    prompt = qtile.widgets_map["prompt"]
    prompt.start_input("Section name: ", layout.cmd_add_section)

# A function for hide/show all the windows in a group
@lazy.function
def minimize_all(qtile):
    for win in qtile.current_group.windows:
        if hasattr(win, "toggle_minimize"):
            win.toggle_minimize()
           
# A function for toggling between MAX and MONADTALL layouts
@lazy.function
def maximize_by_switching_layout(qtile):
    current_layout_name = qtile.current_group.layout.name
    if current_layout_name == 'monadtall':
        qtile.current_group.layout = 'max'
    elif current_layout_name == 'max':
        qtile.current_group.layout = 'monadtall'

keys = [
        ### The essentials
        Key([mod], "Return", lazy.spawn(myTerm), desc='Launches My Terminal'),
        Key([mod, "shift"], "Return", lazy.spawn("dmenu_run -p 'Run: '"), desc='Run Dmenu'),
        Key([mod], "b", lazy.spawn(myBrowser), desc='Librewolf'),
        Key([mod], "Tab", lazy.next_layout(), desc='Toggle through layouts'),
        Key([mod], "q", lazy.window.kill(), desc='Kill active window'),
        Key([mod, "shift"], "r", lazy.restart(), desc='Restart Qtile'),
        Key([mod, "shift"], "c", lazy.shutdown(), desc='Shutdown Qtile'),
        Key([mod, "shift"], "q", lazy.spawn("dm-logout -r"), desc="Logout menu"),
        Key([mod], "r", lazy.spawn(myTerm + "-e ranger"), desc='ranger'),
        Key([mod], "e", lazy.spawn(myFM), desc='pcmanfm'),

        ### Switch focus to specific monitor (out of three)
        Key([mod], "s", lazy.to_screen(0), desc='Keyboard focus to monitor 1'),
        Key([mod], "a", lazy.to_screen(1), desc='Keyboard focus to monitor 2'),
        Key([mod], "d", lazy.to_screen(2), desc='Keyboard focus to monitor 3'),
        
        ### Switch focus of monitors
        Key([mod], "o", lazy.next_screen(), desc='Move focus to next monitor'),
        Key([mod], "p", lazy.prev_screen(), desc='Move focus to prev monitor'),
        
        ### Treetab controls
        Key([mod, "shift"], "Up", 
            lazy.layout.move_left(), 
            desc='Move up a section in treetab'
            ),
        Key([mod, "shift"], "Down",
            lazy.layout.move_right(), 
            desc='Move down a section in treetab'
            ),
        
        # Switch between windows
        # Some layouts like 'monadtall' only need to use j/k to move
        # through the stack, but other layouts like 'columns' will
        # require all four directions h/j/k/l to move around.
        Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
        Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
        Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
        Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
        Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),

        # Same as above but compatibility mode for vim ungifted individuals (me)
        Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
        Key([mod], "Right", lazy.layout.right(), desc="Move focus to right"),
        Key([mod], "Down", lazy.layout.down(), desc="Move focus down"),
        Key([mod], "Up", lazy.layout.up(), desc="Move focus up"),         
        
        # Move windows between left/right columns or move up/down in current stack.
        # Moving out of range in Columns layout will create new column.
        Key([mod, "shift"], "h",
            lazy.layout.shuffle_left(),
            lazy.layout.move_left().when(layout=["treetab"]),
            desc="Move window to the left/move tab left in treetab"),

        Key([mod, "shift"], "l",
            lazy.layout.shuffle_right(),
            lazy.layout.move_right().when(layout=["treetab"]),
            desc="Move window to the right/move tab right in treetab"),

        Key([mod, "shift"], "j",
            lazy.layout.shuffle_down(),
            lazy.layout.section_down().when(layout=["treetab"]),
            desc="Move window down/move down a section in treetab"
        ),
        Key([mod, "shift"], "k",
            lazy.layout.shuffle_up(),
            lazy.layout.section_up().when(layout=["treetab"]),
            desc="Move window downup/move up a section in treetab"
        ),

        # Toggle between split and unsplit sides of stack.
        # Split = all windows displayed
        # Unsplit = 1 window displayed, like Max layout, but still with
        # multiple stack panes
        Key([mod, "shift"], "Tab",
            lazy.layout.rotate(), 
            lazy.layout.flip(), 
            desc='Switch which side main pane occupies (XmonadTall)'
            ),
        Key([mod, "shift"], "space",
            lazy.layout.toggle_split(), 
            desc='Toggle between split and unsplit sides of stack'
            ),

        # Treetab prompt
        Key([mod, "shift"], "a", add_treetab_section, desc='Prompt to add new section in treetab'),


        # Grow/shrink windows left/right. 
        # This is mainly for the 'monadtall' and 'monadwide' layouts
        # although it does also work in the 'bsp' and 'columns' layouts.
        Key([mod], "comma",
            lazy.layout.grow_left().when(layout=["bsp", "columns"]),
            lazy.layout.grow().when(layout=["monadtall", "monadwide"]),
            desc="Grow window to the left"
        ),
        Key([mod], "period",
            lazy.layout.grow_right().when(layout=["bsp", "columns"]),
            lazy.layout.shrink().when(layout=["monadtall", "monadwide"]),
            desc="Grow window to the right"
        ),

        # Grow windows up, down, left, right.  Only works in certain layouts.
        # Works in 'bsp' and 'columns' layout.
        Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
        Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
        Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
        Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
        Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
        Key([mod], "m", lazy.layout.maximize(), desc='Toggle between min and max sizes'),
        Key([mod], "t", lazy.window.toggle_floating(), desc='toggle floating'),
        Key([mod], "f", maximize_by_switching_layout(), lazy.window.toggle_fullscreen(), desc='toggle fullscreen'),
        Key([mod, "shift"], "m", minimize_all(), desc="Toggle hide/show all windows on current group"),
       
        # Dmenu/rofi scripts launched using the key chord SUPER+p followed by 'key'
        KeyChord([mod], "p", [
            Key([], "h", lazy.spawn("dm-hub -r"), desc='List all dmscripts'),
            Key([], "a", lazy.spawn("dm-sounds -r"), desc='Choose ambient sound'),
            Key([], "b", lazy.spawn("dm-setbg -r"), desc='Set background'),
            Key([], "c", lazy.spawn("dtos-colorscheme -r"), desc='Choose color scheme'),
            Key([], "e", lazy.spawn("dm-confedit -r"), desc='Choose a config file to edit'),
            Key([], "i", lazy.spawn("dm-maim -r"), desc='Take a screenshot'),
            Key([], "k", lazy.spawn("dm-kill -r"), desc='Kill processes '),
            Key([], "m", lazy.spawn("dm-man -r"), desc='View manpages'),
            Key([], "n", lazy.spawn("dm-note -r"), desc='Store and copy notes'),
            Key([], "o", lazy.spawn("dm-bookman -r"), desc='Browser bookmarks'),
            Key([], "p", lazy.spawn("rofi-pass"), desc='Logout menu'),
            Key([], "q", lazy.spawn("dm-logout -r"), desc='Logout menu'),
            Key([], "r", lazy.spawn("dm-radio -r"), desc='Listen to online radio'),
            Key([], "s", lazy.spawn("dm-websearch -r"), desc='Search various engines'),
            Key([], "t", lazy.spawn("dm-translate -r"), desc='Translate text')
    ])
]



groups = []
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9",]

group_labels = ["1", "2", "3", "4", "5", "6", "7", "8", "9",]

group_layouts = ["monadtall", "monadtall", "tile", "tile", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall"]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=False),
                desc="Move focused window to group {}".format(i.name),
            ),
        ]
    )



colors = colors.TokyoNightStorm

layout_theme = {"border_width": 2,
                "margin": 8,
                "border_focus": "#f7768e",
                "border_normal": "#6181c5"
                }

layouts = [
    layout.MonadTall(**layout_theme),
    layout.Tile(
        shift_windows=True,
        border_width = 0,
        margin = 0,
        ratio = 0.335,
        ),
    layout.Max(
        border_width = 0,
        margin = 0,
        ),
    layout.Stack(
    	num_stacks=2,
    	border_normal ="#6181c5",
	    border_focus = "#6181c5",
	    margin =1,
	    border_width=2
    	),
    #layout.TreeTab(
    #     font = "Ubuntu Bold",
    #     fontsize = 10,
    #     sections = ["ONE", "TWO", "THREE", "FOUR"],
    #     section_fontsize = 10,
    #     border_width = 2,
    #     bg_color = "#24283b",
    #     active_bg = "#e0af68",
    #     active_fg = "#f7768e",
    #     inactive_bg = "#7dcfff",
    #     inactive_fg = "#c0caf5",
    #     padding_left = 0,
    #     padding_x = 0,
    #     padding_y = 5,
    #     section_top = 10,
    #     section_bottom = 20,
    #     level_shift = 8,
    #     vspace = 3,
    #     panel_width = 200
    #     ),
    #layout.Floating(**layout_theme)
]



# I have no idea what this does
prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())



# Default widget settings
widget_defaults = dict(
    font="Ubuntu Bold",
    fontsize = 10,
    padding = 2,
    background=colors[2]
)

extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
            widget.Spacer(
                    length = 3,
                    background = colors[0]
                    ),
            widget.Image(
                    filename = "~/.config/qtile/icons/icon.png",
                    scale = "False",
                    background = colors[0],
                    mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm)}
                    ),
            widget.Prompt(
                font = "Ubuntu Mono",
                fontsize=14,
                foreground = colors[1]
                    ),
            widget.Spacer(
                    length = 3,
                    background = colors[0]
                    ),
            widget.GroupBox(
                    font = "Ubuntu Bold",
                    fontsize = 12,
                    margin_y = 3,
                    margin_x = 0,
                    padding_y = 5,
                    padding_x = 3,
                    borderwidth = 3,
                    active = colors[8], #Workspace in use
                    inactive = colors[2], #Workspaces not in use
                    rounded = False,
                    highlight_color = colors[0], #Highlight over workspace
                    highlight_method = "line",
                    this_current_screen_border = colors[8], #Line on focused screen
                    this_screen_border = colors [1], #Line on unfocused screen
                    other_current_screen_border = colors[8], #Line on other screens focused
                    other_screen_border = colors[1], #Line on other screens unfocused
                    foreground = colors[2],
                    background = colors[0]
                    ),
            widget.TextBox(
                    text = '|',
                    font = "Ubuntu Mono",
                    background = colors[0],
                    foreground = colors[2],
                    padding = 2,
                    fontsize = 14
                    ),
            widget.CurrentLayoutIcon(
                    custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                    foreground = colors[2],
                    background = colors[0],
                    padding = 0,
                    scale = 0.7
                    ),
            widget.CurrentLayout(
                    foreground = colors[5],
                    background = colors[0],
                    padding = 5
                    ),
            widget.TextBox(
                    text = '|',
                    font = "Ubuntu Mono",
                    background = colors[0],
                    foreground = colors[2],
                    padding = 2,
                    fontsize = 14
                    ),
            widget.WindowName(
                    foreground = colors[3],
                    background = colors[0],
                    padding = 0,
                    parse_text = longNameParse
                    ),
            widget.Systray(
                    background = colors[0],
                    padding = 5,
                    icon_size = 10
                    ),
            widget.Spacer(
                    length = 6,
                    background = colors[0]
                    ),
            #widget.Cmus(
            #         font = "Ubuntu Mono",
            #         padding = 0,
            #         fontsize = 10,
            #         noplay_color = '83615b',
            #         play_color = '83615b',
            #         decorations = [
            #  	           BorderDecoration (
            #              colour = colors[1],
            #              border_width =  [0, 0, 2, 0],
            #              padding_x = 5,
            #              padding_y = None,)
            #       ],
            #       ),	   
	        #Spotify(
            #  	play_icon = "󰎇",
            #  	pause_icon = "",
            #  	font = "Ubuntu Bold ",
            #      padding = 0,
            #      fontsize = 10,
            # 		size = 24,
            #   	format = "{icon} {artist}:{album} - {track}",
            #   	update_interval = "0.5",
            #   	background = colors[0],
            #   	decorations = [
            #            BorderDecoration (
            #            colour = colors[1],
            #            border_width =  [0, 0, 2, 0],
            #            padding_x = None,
            #            padding_y = None,)
            #       ],
            #       ),
	       widget.Mpris2(
	       		    font = "Ubuntu Bold",
                    padding = 3, 
                    width = 150,
	       	 	    foreground = colors[3],
		            background = colors[0],
		            name = "spotify",
		            objname = "org.mpris.MediaPlayer2.spotify",
		            paused_text = "      ",
		            display_metadata = ["󰎇", "xesam:title", "xesam:artist"],
		            scroll_chars = 45,
		            scroll_interval = 0.3,
		            decorations = [
                       	BorderDecoration (
                       	colour = colors[3],
                      	border_width =  [0, 0, 2, 0],
                      	padding_x = None,
                      	padding_y = None,)
              	       ],
		               ),
            widget.Spacer(
                    length = 6,
                    background = colors[0]
                    ),
            widget.CheckUpdates(
                    #no_update_string='󰚰 No updates',
                    update_interval = 1800,
                    distro = "Arch_checkupdates",
                    display_format = "󰚰 Updates: {updates} ",
                    foreground = colors[4],
                    colour_have_updates = colors[4],
                    colour_no_updates = colors[4],
                    mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e sudo pacman -Syu')},
                    padding = 5,
                    background = colors[0],
                    decorations = [
                        BorderDecoration (
                       	colour = colors[4],
                       	border_width =  [0, 0, 2, 0],
                       	padding_x = None,
                       	padding_y = None,)
                       ],
                       ),
            widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[0],
                       background = colors[0]
                       ),
            widget.CPU(
                       fmt = '{}',
                       format = ' Cpu: {freq_current}GHz {load_percent}%',
                       font = "Ubuntu Bold",
                       foreground = colors[7],
                       background = colors[0],
                       padding = 5,
                       decorations = [
                       		BorderDecoration (
                       		colour = colors[7],
                       		border_width =  [0, 0, 2, 0],
                       		padding_x = None,
                       		padding_y = None,)
                       ], 
                       ),
            widget.Memory(
                       foreground = colors[5],
                       background = colors[0],
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                       fmt = '    Mem: {}',
                       padding = 5,
                       decorations = [
                       		BorderDecoration (
                       		colour = colors[5],
                       		border_width =  [0, 0, 2, 0],
                       		padding_x = None,
                       		padding_y = None,)
                       ],
                       ),
            widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[0],
                       background = colors[0]
                       ),
            widget.ThermalSensor(
                       foreground = colors[6],
                       background = colors[0],
                       threshold = 90,
                       fmt = ' Temp: {}',
                       padding = 5,
                       decorations = [
                       		BorderDecoration (
                       		colour = colors[6],
                       		border_width =  [0, 0, 2, 0],
                       		padding_x = None,
                       		padding_y = None,)
                       ], 
                       ),
            widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[0],
                       background = colors[0]
                       ),
            widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[0],
                       background = colors[0]
                       ),
            widget.Volume(
                        foreground = colors[8],
                        background = colors[0],
                        fmt = '  Vol: {}',
                        volume_app = 'pavucontrol',
                        step = '2',
                        padding = 5,
                        decorations = [
                       		BorderDecoration (
                       		colour = colors[8],
                       		border_width =  [0, 0, 2, 0],
                       		padding_x = None,
                       		padding_y = None,)
                       ],
                       ),
            widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[0],
                       background = colors[0]
                       ),
            widget.Clock(
                       foreground = colors[9],
                       background = colors[0],
                       format = "    %a, %b %d - %H:%M ",
                       decorations = [
                       		BorderDecoration (
                       		colour = colors[9],
                       		border_width =  [0, 0, 2, 0],
                       		padding_x = None,
                       		padding_y = None,)
                       ],
                       ),
            widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[0],
                       background = colors[0]
                       ),
		]
    return widgets_list



def longNameParse(text):
	for string in ["LibreWolf", "Firefox"]: #Shortens app names in WindowName widget
		if string in text:
	   		text = string
		else:
	  		text = text
	return text



def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    del widgets_screen2[9:10]               # Slicing removes unwanted widgets (systray) on Monitors 1,3
    return widgets_screen2

def init_widgets_screen1():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2                 # Monitor 2 will display all widgets in widgets_list



# For adding transparency to your bar, add (background="#00000000") to the "Screen" line(s)
# For ex: Screen(top=bar.Bar(widgets=init_widgets_screen2(), background="#00000000", size=24)),
def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=20, margin=[4,8,1,8])),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=1.0, size=20, margin=[4,8,1,8])),
            Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=20, margin=[4,8,1,8]))]



if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(
    #border_focus=colors[8],
    border_width=2,
    float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),   # gitk
        Match(wm_class="dialog"),         # dialog boxes
        Match(wm_class="download"),       # downloads
        Match(wm_class="error"),          # error msgs
        Match(wm_class="file_progress"),  # file progress boxes
        Match(wm_class='kdenlive'),       # kdenlive
        Match(wm_class="makebranch"),     # gitk
        Match(wm_class="maketag"),        # gitk
        Match(wm_class="notification"),   # notifications
        Match(wm_class='pinentry-gtk-2'), # GPG key password entry
        Match(wm_class="ssh-askpass"),    # ssh-askpass
        Match(wm_class="toolbar"),        # toolbars
        Match(wm_class="Yad"),            # yad boxes
        Match(title="branchdialog"),      # gitk
        Match(title='Qalculate!'),        # qalculate-gtk
        Match(title="pinentry"),          # GPG key password entry
])

auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "qtile"