TokyoNightStorm =[
	["#24283b", "#24283b"],  # panel background color
	["#6181c5", "#6181c5"],  # colors for workspace in use
	["#c0caf5", "#c0caf5"],  # colors for current selected workspace
	["#f7768e", "#f7768e"],  # colors for 1st right widget
	["#9ece6a", "#9ece6a"],  # colors for 2nd right widget (hidden by default)
	["#e0af68", "#e0af68"],  # colors for 3rd right widget
	["#7aa2f7", "#7aa2f7"],  # colors for 4th right widget
	["#bb9af7", "#bb9af7"],  # colors for 5th right widget and app title
	["#7dcfff", "#7dcfff"],  # colors for 6th right widget
	["#a9b1d6", "#a9b1d6"]   # colors for 7th right widget
	]