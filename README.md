# DOTFILES

---

## Notes:

---

I have made these configs on Arch based distros so there's no guarantee it will work exactly the same on other distros.
- Check out my [install script](https://gitlab.com/viperrnya/install-script) to automatically install my desktop environment



## Configs

---

- [Qtile](https://docs.qtile.org/en/latest/manual/install/index.html)
- [Alacritty](https://github.com/alacritty/alacritty)
- [dmenu](https://tools.suckless.org/dmenu/)
- [Starship Prompt](https://github.com/starship/starship)
- [Neofetch](https://github.com/dylanaraps/neofetch)
- [Picom](https://github.com/yshui/picom)
- [Nitrogen](https://github.com/l3ib/nitrogen)
- .bashrc



## Dependencies

---

- [Qtile Extras](https://qtile-extras.readthedocs.io/en/stable/manual/install.html)
- [psutil](https://pypi.org/project/psutil/)
- [dbus-next](https://pypi.org/project/dbus-next/)
- [ttf-ubuntu-font-family](https://archlinux.org/packages/extra/any/ttf-ubuntu-font-family/)
- [ttf-font-awesome](https://archlinux.org/packages/extra/any/ttf-font-awesome/)
- [ttf-cascadia-code-nerd](https://archlinux.org/packages/extra/any/ttf-cascadia-code-nerd/)
- [Shell Color Scripts](https://gitlab.com/dwt1/shell-color-scripts)
- [Dmscripts](https://gitlab.com/dwt1/dmscripts)
- [htop](https://github.com/htop-dev/htop)
- [pavucontrol](https://archlinux.org/packages/extra/x86_64/pavucontrol/)
- [ranger](https://github.com/ranger/ranger)



## Keybindings (Qtile) - inaccurate, requires updating

---

Default apps
  - Terminal - [Alacritty](https://github.com/alacritty/alacritty)
  - Browser - [Librewolf](https://librewolf.net/installation/)
  - File Manager - [pcmanfm](https://archlinux.org/packages/extra/x86_64/pcmanfm/)

---

| Key Combination (Mod+Key) |             Function             |
|:-------------------------:|:--------------------------------:|
| Super                     | Mod                              |
| Return                    | Spawn Terminal                   |
| Shift + Return            | Launch dmenu                     |
| B                         | Launch Browser                   |
| Tab                       | Toggle Layout                    |
| Q                         | Kill Window                      |
| Shift + R                 | Restart Qtile                    |
| Shift + Q                 | Kill Qtile                       |
| R                         | Launch Ranger                    |
| E                         | Launch File Manager              |
| A                         | Focus Screen 1                   |
| S                         | Focus Screen 2                   |
| D                         | Focus Screen 3                   |
| Period                    | Focus Next Screen                |
| Comma                     | Focus Prev Screen                |
| ↑ ↓ ← →                   | Focus Window ↑ ↓ ← →             |
| Shift + ↑ ↓ ← →           | Move Window ↑ ↓ ← →              |
| CTRL + ↑                  | Expand Window                    |
| CTRL + ↓                  | Shrink Window                    |
| N                         | Normalize Window Size Ratios     |
| M                         | Toggle Between Min And Max Sizes |
| Shift + F                 | Toggle Floating Windows          |
| F                         | Toggle Fullscreen                |
| 1 - 8                     | Change Workspace                 |
| Shift + 1 - 8             | Move Window to Workspace         |

**Notes:**
  - There are more key combinations that are layout specific or dmenu related that you can find in the Qtile config.
  - I'm going to switch arrow keys to vim keys eventually, shush


## Todo

---

- [x] Add more aliases to .bashrc and make it more readable
- [ ] Remove some widgets from side monitors
- [x] Make script to install all basic apps and set up the DE
- [x] Change color scheme to be more legible
- [ ] Add config to automatically apply dark GTK and QT themes
- [ ] Try out the Fish shell
- [x] Improve Starship config
- [ ] Add conky
- [x] Remake the qtile config