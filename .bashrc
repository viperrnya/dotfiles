### .bashrc ###

### If not running interactively, don't do anything ###
[[ $- != *i* ]] && return



### Prompt Settings ###
#PS1='[\u@\h \W]\$ ' # Default Prompt

eval "$(starship init bash)" # Starship Prompt

colorscript random; # Enable Colorscript


#(cat ~/.cache/wal/sequences &) # Enable Pywal



### Aliases ###
bind "set completion-ignore-case on" # Ignore upper and lowercase when TAB completion

#alias sudo=doas # Replace sudo with doas


# Shortcuts #
alias ls='ls -la --color=auto' 

alias vim='nvim'

# Navigating Directories #
alias dow='cd ~/Downloads' 

alias b='cd ..'

alias qtl='cd ~/.config/qtile/'



# Pacman and Yay #
alias pacsyu='sudo pacman -Syu'                  # update only standard pkgs
alias pacsyyu='sudo pacman -Syyu'                # Refresh pkglist & update standard pkgs
alias yaysua='yay -Sua --noconfirm'              # update only AUR pkgs (yay)
alias yaysyu='yay -Syu --noconfirm'              # update standard pkgs and AUR pkgs (yay)
#alias unlock='sudo rm /var/lib/pacman/db.lck'    # remove pacman lock
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)' # remove orphaned packages


# Colorize grep output #
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'


# Confirm before overwriting something #
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'


# gpg encryption #
 # verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
 # receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"


# Bare git repo alias for dotfiles #
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'



### ARCHIVE EXTRACTION ###
# usage: ex <file>
ex ()
{
  if [ -f "$1" ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
